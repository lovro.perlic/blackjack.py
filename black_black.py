import random
from player import Player
import json




suits = ['♥', '♦', '♣', '♠']
ranks = ['Ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King']

def create_deck():
    deck = []
    for suit in suits:
        for rank in ranks:
            card = (rank, suit)
            deck.append(card)
    return deck

deck = create_deck()
random.shuffle(deck)
current_card = 0

def draw_card(deck, current_card):
    card = deck[current_card][0]
    current_card += 1
    return card, current_card

#card, current_card = draw_card(deck, current_card)



def place_bet(balance, bet):
    bet_flag = True

    while bet_flag:
        bet = int(input('Unesite vaš ulog'))
        if bet <= balance & balance > 0:
            bet_flag = False

    balance = balance - bet
    return balance

def is_name_in_json(name, path_to_json):
    with open(path_to_json, 'r', encoding='utf-8') as f:
        players = json.load(f)
    for player in players:
        if player['Player'] == name:
            return player['Player'], player['Balance']
    return None, None

path_to_json = 'players.json'


name = input('Unesite svoje ime:')
temp_name = name
name, balance = is_name_in_json(name, path_to_json)

if name is not None:
    print(f'Dobro došao nazad {name}! imaš {balance} novaca.')
else:
    name = temp_name
    balance = 10000
    print(f'Dobrodošli {name} u Black Jack!')
    print(f'Imate {balance} novaca')

player_plays = Player (name, balance)

player_plays.save_to_json(path_to_json)




