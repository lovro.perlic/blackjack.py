import json
name = ""
balance = 0


class Player:
    def __init__(self, name, balance):
        self.name = name
        self.balance = balance
        self.hand = []

    def __str__(self):
        return f'{self.name}, {self.balance}, {self.hand}'


    def hit(self, card):
        self.hand.append(card)

    def double_down(self, card):
        self.balance -= bet
        bet *= 2
        self.hit(card)

    def split(self):
        hand1 = [self.hand[0]]
        hand2 = [self.hand[1]]

    def stand(self):
        pass

    def load_from_json(self, path_to_json):
        with open(path_to_json, 'r', encoding='utf-8') as f:
            players = json.load(f)
        for line in players:
            p = Player(line["Player"], line["Balance"])
            self.players.append(p)
        return

    def save_to_json(self, path_to_json):
        list_of_dictionaries =[]
        players_dictionary = []
        with open(path_to_json, 'w', encoding='utf-8') as f:
            for p in self.players:
                players_dictionary["Player"] = p.name
                players_dictionary["Balance"] = p.balance
                list_of_dictionaries.append(players_dictionary.copy())
                

    def print_players(self):
        for p in self.players:
            print(p)